> [!IMPORTANT]  
> This repo is deprecated - OpsLevel now natively pulls in deploys based on your usage for Gitlab deploy environments and setting up the [Gitlab integration](https://docs.opslevel.com/docs/gitlab-self-managed#gitlab-saas).  



[![Overall](https://img.shields.io/endpoint?style=flat&url=https%3A%2F%2Fapp.opslevel.com%2Fapi%2Fservice_level%2Fo6yqmG-TV9lw233O6wdtQh7JcqI-bPVkYpZ9yfO-NKM)](https://app.opslevel.com/services/report_deploy_gitlab_ci/maturity-report)

# Report Deploy GitLab CI

This is a GitLab CI Reusable Pipeline that can be used to report Deploy Events to OpsLevel.

## Getting started

Step 1/2: In your GitLab project's `.gitlab-ci.yml`, add the Reusable Pipeline:

```
include:
  - project: 'opslevel/report-deploy-gitlab-ci'
    ref: v0.1.5
    file: '/report-deploy.gitlab-ci.yml'
    inputs:
      job-stage: deploy     # the pipeline will run in the `deploy` stage.
      needs:
        - deploy-job-1      # the pipeline will not run in the `deploy` stage unless
        - deploy-job-2      # both of these jobs finish.
      rules:
        - if: $CI_MERGE_REQUEST_IID || $CI_COMMIT_BRANCH == "main"
          allow_failure: true

      name: prod-env-1      # pipeline will be named `opslevel-report-deploy-prod-env-1`
      retry: 3

      # set input variables for deploy event body
      description: this deploy event was reported by the OpsLevel GitLab CI reusable pipeline!
      environment: production
      integration_url: $YOUR_MASKED_TOKEN_IN_GITLAB
      service_alias: my_service
```

**TIP:** you can `include` the Reusable Pipeline multiple times to report deploys on multiple services/environments.

[Full list of configurable variables](./report-deploy.gitlab-ci.yml)

Step 2/2: Run your project's pipeline and check the list of deploys in OpsLevel once complete.

![Example of a deploy event sent using this Reusable Pipeline](deploys.png)

## Reference

[OpsLevel Docs - Get Started with the GitLab CI integration](https://docs.opslevel.com/docs/gitlab-ci-integration)

[OpsLevel Docs - Custom Deploys](https://docs.opslevel.com/docs/deploys)

[GitLab Doc - How to build more reusable CI/CD templates](https://about.gitlab.com/blog/2023/05/01/how-to-build-reusable-ci-templates/)

[GitLab Doc - CI/CD YAML syntax reference](https://docs.gitlab.com/ee/ci/yaml/)

[Sam Kohlleffel - Creating Reusable CI/CD Pipelines with GitLab](https://medium.com/hashmapinc/creating-reusable-ci-cd-pipelines-with-gitlab-4c898de30b1e)

This Reusable Pipeline uses [the OpsLevel CLI under the hood](https://github.com/OpsLevel/cli) to report Deploy Events.
